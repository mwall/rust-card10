#![no_std]
#![no_main]

use core::fmt::{self, Write};
use card10_l0dable::*;

main!(main);

fn main() {
    let result = run();
    if let Err(error) = result {
        writeln!(UART, "error: {}\r", error);
    }
}

fn run() -> Result<(), Error>  {
    let bhi_acc = BHI160::<Accelerometer>::start()?;
    loop {
        for d in &bhi_acc.read()? {
            writeln!(UART, "BHI {:?}\r", d).unwrap();
        }
    }
}

// -----------------------------------------------------------------------------
// Error
// -----------------------------------------------------------------------------

#[derive(Debug)]
pub enum Error {
    UartWriteFailed(fmt::Error),
    SensorInteractionFailed(BHI160Error),
}

impl From<fmt::Error> for Error {
    fn from(error: fmt::Error) -> Self {
        Error::UartWriteFailed(error)
    }
}

impl From<BHI160Error> for Error {
    fn from(error: BHI160Error) -> Self {
        Error::SensorInteractionFailed(error)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::UartWriteFailed(error) => error.fmt(f),
            Error::SensorInteractionFailed(error) => error.fmt(f),
        }
    }
}
