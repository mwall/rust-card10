use core::fmt::Write;
use card10_l0dable::*;

pub struct LinePlot<'a>  {
    min: isize,
    max: isize,
    color: Color,
    values: [u16; Display::W as usize],
    disp: &'a Display,
}

impl<'a> LinePlot<'a> {    
    pub fn plot(&self){
        for i in 0..self.values.len(){
            if i > 0{
                self.disp.line((i-1) as u16, self.values[i-1],i as u16,self.values[i],self.color,LineStyle::Full,1);
            }
        }
    }

    fn roll_array(&mut self, new_value: u16) {
        for i in 0..self.values.len(){
            if i < self.values.len() - 1{
                self.values[i] = self.values[i+1];
            }else{
                self.values[i] = new_value;
            }
        };
    }

    fn scale(&self,v: isize) -> u16 {
        if v > self.max || v < self.min{
            writeln!(UART, "Warrning: Value out of range {:?}, unable to plot ..: ", v);
            Display::H * 2 as u16
        }else {
            let k = v - self.min;
            let f = k as f32 / (self.max-self.min) as f32;
            Display::H - (f * Display::H as f32) as u16
        }
    }

    pub fn update(&mut self, new_value: isize){
        let scaled_value = self.scale(new_value);
        self.roll_array(scaled_value);
    }

    pub fn new(min: isize, max: isize, color: Color, disp: &'a Display) -> Self {
        let values: [u16; Display::W as usize] = [Display::H as u16; Display::W as usize];
        Self { min, max, color, values, disp }
    }
}
