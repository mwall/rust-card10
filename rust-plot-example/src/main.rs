#![no_std]
#![no_main]

use core::fmt::{self, Write};
use card10_l0dable::*;

mod line_plot;
main!(main);

enum View{
    WELCOMEVIEW,
    PLOTVIEW,
    INFOVIEW,
}

enum SensorType{
    BME680_GAS,
    BHI160_ACC,
}

const SENSOR_ORDER: [SensorType;2] = [SensorType::BME680_GAS, SensorType::BHI160_ACC];


fn plot_welcome_screen(display: &Display){
    // first row
    display.print(0, 0, b"BME-sensor\0", Color::white(), Color::black());
    display.
    // second row
    display.print(0, 20, b"visuals\0", Color::white(), Color::black());

    // third row
    display.print(0, 40, b"<\0", Color::red(), Color::black());
    display.print(10, 40, b"Info\0", Color::white(), Color::black());
    display.print(Display::W-70,40, b"exit\0", Color::white(), Color::black());
    display.print(Display::W-14,40, b">\0", Color::red(), Color::black());

    // fourth row
    display.print(Display::W-14,60, b">\0", Color::red(), Color::black());
    display.print(10, 60, b"chg sensor\0", Color::white(), Color::black());
}

fn main() {
    let result = run();
    if let Err(error) = result {
        writeln!(UART, "error: {}\r", error);
    }
}

fn run() -> Result<(), Error>  {
    let display = Display::open();
    let bme_sensor = BME680::start();
    let bhi_acc = BHI160::<Accelerometer>::start()?;

    let mut gas_plot = line_plot::LinePlot::new(0,500000,Color::blue(),&display);
    
    let mut acc_plot_x = line_plot::LinePlot::new(-100,100,Color::red(),&display);
    let mut acc_plot_y = line_plot::LinePlot::new(-100,100,Color::black(),&display);
    let mut acc_plot_z = line_plot::LinePlot::new(-100,100,Color::green(),&display,);

    let mut view = View::WELCOMEVIEW;
    let mut curr_sensor = &SENSOR_ORDER[0];
    
    loop{
        display.clear(Color::white());
        let bme_data = bme_sensor.read().unwrap();
        gas_plot.update(bme_data.gas_resistance as isize);
        for d in &bhi_acc.read()? {
            writeln!(UART, "BHI {:?}\r", d).unwrap();
            
            acc_plot_x.update((d.x * 100.0) as isize);
            acc_plot_y.update((d.y * 100.0) as isize);
            acc_plot_z.update((d.z * 100.0) as isize);
        }

        match view{
            View::WELCOMEVIEW => {
                plot_welcome_screen(&display);        
            }
            View::PLOTVIEW => {
                match curr_sensor{
                    SensorType::BME680_GAS => {
                        gas_plot.plot();
                    }
                    SensorType::BHI160_ACC => {
                        acc_plot_x.plot();
                        acc_plot_y.plot();
                        acc_plot_z.plot();
                    }
                }
            }
            View::INFOVIEW => {
                match curr_sensor{
                    SensorType::BME680_GAS => {
                        display.print(0, 0, b"BME-Sensor:\0", Color::red(), Color::black());
                        display.print(0, 20, b"gas pressure\0", Color::red(), Color::black());
                    }
                    SensorType::BHI160_ACC => {
                        display.print(0, 0, b"BHI-Sensor:\0", Color::red(), Color::black());
                        display.print(0, 20, b"accelerometer\0", Color::red(), Color::black());
                    }
                }
            }
        }
        let b = Buttons::read();
        if b.left_bottom() {
            view = match view{
                View::WELCOMEVIEW => View::PLOTVIEW,
                View::PLOTVIEW => View::INFOVIEW,
                View::INFOVIEW => View::PLOTVIEW,
            }
        }
        if b.right_top() {
            exit(0);
        }
        if b.right_bottom() {
            curr_sensor = match curr_sensor{
                SensorType::BHI160_ACC => &SensorType::BME680_GAS,
                SensorType::BME680_GAS => &SensorType::BHI160_ACC,
            }
        }
        display.update();
    };
}

// -----------------------------------------------------------------------------
// Error
// -----------------------------------------------------------------------------

#[derive(Debug)]
pub enum Error {
    UartWriteFailed(fmt::Error),
    SensorInteractionFailed(BHI160Error),
}

impl From<fmt::Error> for Error {
    fn from(error: fmt::Error) -> Self {
        Error::UartWriteFailed(error)
    }
}

impl From<BHI160Error> for Error {
    fn from(error: BHI160Error) -> Self {
        Error::SensorInteractionFailed(error)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::UartWriteFailed(error) => error.fmt(f),
            Error::SensorInteractionFailed(error) => error.fmt(f),
        }
    }
}
